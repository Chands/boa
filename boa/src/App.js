import "./App.css";

import { useSelector, useDispatch } from "react-redux";
import { Fragment } from "react";
import { Route, Switch } from "react-router-dom";
import { activeUserActions } from "../src/store/activeUser";
import { userActions } from "./store/user";
import { useHistory } from "react-router-dom";

import Header from "./components/header2/Header";
import Footer from "./components/footer/Footer";
import StudentPanel from "./components/student/StudentPanel";
import ParentPanel from "./components/parent/ParentPanel";
import PanelButton from "./components/ui/PanelButton";
import AppButton from "./components/ui/AppButton";

function App() {
  const dispatch = useDispatch();
  const history = useHistory();

  const cookiesAccepted = useSelector(
    (state) => state.activeUser.cookiesAccepted
  );

  const cookiesAcceptedHandler = () => {
    dispatch(activeUserActions.setCookiesAccepted());
    history.push("/"); // Return user to home panel
  };

  // Set background colour of page
  const activeUser = useSelector((state) => state.activeUser.activeUser);
  switch (activeUser) {
    case "none":
      document.body.style.backgroundColor = "#435B9D";
      break;
    case "parent":
      document.body.style.backgroundColor = "#FF5964";
      break;
    case "student":
      document.body.style.backgroundColor = "#81B757";
      break;
    default:
      document.body.style.backgroundColor = "#435B9D";
      break;
  }

  // Initialise user using local storage
  const initialiseUser = () => {
    // Check for parent password, set to default if it doesn't exist
    if (localStorage.getItem("parentPassword") === null) {
      dispatch(userActions.setParentPassword("boa"));
    } else {
      dispatch(
        userActions.setParentPassword(localStorage.getItem("parentPassword"))
      );
    }
  };

  // Run function
  initialiseUser();

  return (
    <Fragment>
      <Header />
      <div className="central-body">
        {cookiesAccepted ? (
          <Switch>
            <Route path="/student">
              <StudentPanel />
            </Route>
            <Route path="/parent">
              <ParentPanel />
            </Route>
            <Route path="/">
              <div className="panel-container">
                <PanelButton
                  color="green"
                  text="Student"
                  link="/student"
                  image="Snake_Child.png"
                  alt="Link to the Student Panel"
                />
                <PanelButton
                  color="red"
                  text="Parent"
                  link="/parent"
                  image="Snake_Parent.png"
                  alt="Link to the Parent Panel"
                />
              </div>
            </Route>
          </Switch>
        ) : (
          <Fragment>
            <h2>
              This application uses local storage to provide website
              functionality and save information about your learning. Please
              click the button below to accept its usage as part of this
              application.
            </h2>
            <AppButton
              onClick={cookiesAcceptedHandler}
              color="blue"
              text="Accept"
            />
          </Fragment>
        )}
      </div>
      <Footer />
    </Fragment>
  );
}

export default App;
