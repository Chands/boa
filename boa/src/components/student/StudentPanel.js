import { Fragment } from "react";
import { Switch, Route, useRouteMatch } from "react-router-dom";
import { useDispatch } from "react-redux";
import PanelButton from "../ui/PanelButton";
import { activeUserActions } from "../../store/activeUser";
import { useEffect } from "react";
import PractiseExam from "./PractiseExam";
import Quiz from "./quiz/Quiz";
import ActivitySubmit from "./activity/ActivitySubmit";
import classes from "./StudentProfile.module.css";
import snakeImage from "../../assets/Snake_Child.png";
import AppButton from "../ui/AppButton";

const StudentPanel = () => {
  const dispatch = useDispatch();

  // Update the active user to be a student
  useEffect(() => {
    dispatch(activeUserActions.setActiveUser("student"));
  }, [dispatch]);

  let match = useRouteMatch();

  // Load point total from local storage
  let studentPoints = JSON.parse(localStorage.getItem("points"));
  if (studentPoints == null) {
    // Set to empty array if it doesn't exist
    studentPoints = 0;
  }

  // Levels
  // Each in array represents the threshold to go up a level
  let levels = [
    0, 10, 20, 40, 70, 110, 160, 220, 280, 360, 450, 550, 650, 750, 850, 1000,
  ];
  let studentLevel = 0;
  let toNextLevel = 0;

  // Get player level
  levels.forEach((level, index) => {
    if (studentPoints >= level) {
      studentLevel = index;
      toNextLevel = levels[index + 1] - studentPoints;
    } else {
      // Do nothing
    }
  });

  // Determine student title
  let studentTitle = "";
  switch (true) {
    case studentLevel <= 2 && studentLevel >= 0:
      studentTitle = "Hatchling";
      break;
    case studentLevel <= 5 && studentLevel > 2:
      studentTitle = "Python";
      break;
    case studentLevel <= 8 && studentLevel > 5:
      studentTitle = "Kingsnake";
      break;
    case studentLevel <= 11 && studentLevel > 8:
      studentTitle = "Viper";
      break;
    case studentLevel <= 14 && studentLevel > 11:
      studentTitle = "Cobra";
      break;
    case studentLevel === 15:
      studentTitle = "King Cobra";
      break;
    default:
      break;
  }

  // Width of level bar
  let barPercentage = (studentPoints / (studentPoints + toNextLevel)) * 100 + '%';

  return (
    <Fragment>
      <Switch>
        <Route path={`${match.path}/exam`}>
          <PractiseExam />
        </Route>
        <Route path={`${match.path}/practise`}>
          <Quiz />
        </Route>
        <Route path={`${match.path}/activity`}>
          <ActivitySubmit />
        </Route>
        <Route path={`${match.path}`}>
          <h1>Student Panel</h1>
          <p>What would you like to do?</p>
          <div className="panel-container">
            <PanelButton
              color="green"
              text="Take an examination."
              link={`${match.path}/exam`}
              image="Exam_Icon.svg"
              alt="Link to take an exam"
            />
            <PanelButton
              color="green"
              text="Practise questions."
              link={`${match.path}/practise`}
              image="Quiz_Icon.svg"
              alt="Link to complete a quiz"
            />
            <PanelButton
              color="green"
              text="Submit an activity."
              link={`${match.path}/activity`}
              image="Activity_Icon.svg"
              alt="Link to submit activites you have completed"
            />
          </div>
          <div className={classes.container}>
            {/* <p>Your Profile:</p>
            <p>You have {studentPoints} points!</p>
            <p>You are Level {studentLevel}.</p>
            <p>{toNextLevel} to next level.</p>*/}
            <div className={classes.profileLeft}>
              <div className={classes.profileImageContainer}>
                <img src={snakeImage} alt="The student's avatar" />
              </div>
              <div className={classes.profileTitle}>
                <h1>{studentTitle}</h1>
              </div>
            </div>
            <div className={classes.profileRight}>
              <div className={classes.levelBar}>
                <div className={classes.levelBarFill} style={{width: barPercentage}}></div>
                <h2>Level {studentLevel}</h2>
              </div>
              <div className={classes.profileInfo}>
                <h1>Your Profile:</h1>
                <p>You have {studentPoints} points!</p>
                <p>{toNextLevel} to next level.</p>
                <AppButton color="green" text="Customize" />
              </div>
            </div>
          </div>
        </Route>
      </Switch>
    </Fragment>
  );
};

export default StudentPanel;
