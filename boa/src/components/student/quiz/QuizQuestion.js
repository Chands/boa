import { Fragment } from "react";
import classes from "../exam/submission/MarkExam.module.css";

const QuizQuestion = (props) => {
  const image = props.question.image;
  // const imageMobile = props.question.imageMobile;
  const questionNumber = props.questionNumber;
  const options = props.question.options;
  const primaryInstruction = props.question.primaryInstruction;
  const secondaryInstruction = props.question.secondaryInstruction;
  const textExtract = props.question.textExtract;

  // Type of question
  const type = props.question.type;

  const multipleChoiceAnswerHandler = (event) => {
    props.updateAnswers(event.target.value, questionNumber);
  };

  const multipleChoiceAnswerHandlerDiv = (value) => {
    props.updateAnswers(value, questionNumber);
  };

  // For multiple choice, this is to display each option
  const populateMultipleChoice = () => {
    let returnStatement = "";
    returnStatement = options.map((option, index) => {
      return (
        <div
          className="multiple-choice-option-container"
          onClick={(e) => multipleChoiceAnswerHandlerDiv(option)}
          key={index}
        >
          <input
            name={option}
            type="checkbox"
            value={option}
            id={index}
            onChange={multipleChoiceAnswerHandler}
            className="checkbox-input"
          />
            <p>{option}</p>
        </div>
      );
    });

    return returnStatement;
  };

  // Setup the input options based on the type of question
  const populateInput = () => {
    let returnStatement = null;
    switch (type) {
      case "text":
        // When the input field is exited, submit the answer. Don't allow enter to refresh the page
        returnStatement = (
          <form onSubmit={(e) => e.preventDefault()}>
            <input
              className="green-text-input"
              type="text"
              placeholder="Enter your answer here:"
              onChange={(e) =>
                props.updateAnswers(e.target.value, questionNumber)
              }
            />
          </form>
        );
        break;
      case "multipleChoice":
        // For this one, populate the form with a select field with each possible option
        returnStatement = (
          <form onSubmit={(e) => e.preventDefault()}>
            {populateMultipleChoice()}
          </form>
        );
        break;
      default:
    }

    return returnStatement;
  };

  return (
    <Fragment>
      <div key={Math.random()} className={classes.answerDiv}>
        <h2>Question Number: {questionNumber + 1}.</h2>
        <p>{primaryInstruction}</p>
        <div className="questionImageContainer">
          <img
            src={require(`../../../assets/exams${image}`)}
            alt="Question's associated figure."
          ></img>
        </div>
        {/* If text extract exists (for english exams) then put it in */}
        {textExtract !== "" && <p>{textExtract}</p>}
        <p>{secondaryInstruction}</p>
        {/* Answer form */}
        {populateInput()}
      </div>
    </Fragment>
  );
};

export default QuizQuestion;
