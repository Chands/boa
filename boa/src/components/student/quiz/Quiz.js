import { Fragment, useState, useEffect } from "react";
import exams from "../../../assets/exams/exams.json";
import CategoryButton from "../../ui/CategoryButton";
import classes from "../../student/exam/ExamList.module.css";
import QuizQuestion from "./QuizQuestion";
import MarkQuiz from './MarkQuiz';
import AppButton from "../../ui/AppButton";

const Quiz = () => {
  // Before beginning the quiz
  const [startQuiz, setStartQuiz] = useState(false);
  const [subjectSelected, setSubjectSelected] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [questions, setQuestions] = useState([]);
  const [questionAnswers, setQuestionAnswers] = useState([]);
  const [userAnswers, setUserAnswers] = useState(["", "", "", "", ""]);
  const [quizCompleted, setQuizCompleted] = useState(false);

  useEffect(() => {
    if (subjectSelected) {
      // Set loading state as it reads data
      setIsLoading(true);

      // Depending on the subject, create an array of possible questions and select 5 of them
      let possibleQuestions = [];
      let possibleAnswers = [];
      switch (subjectSelected) {
        case "vr":
          exams.papers.forEach((exam) => {
            // For every question in each exam, add to the possible questions array, only if VR
            if (exam.subject === "vr") {
              exam.questions.forEach((question) => {
                possibleQuestions.push(question);
              });
              exam.answers.forEach((answer) => {
                possibleAnswers.push(answer);
              });
            }
          });
          break;
        case "n-vr":
          exams.papers.forEach((exam) => {
            // For every question in each exam, add to the possible questions array, only if N-VR
            if (exam.subject === "n-vr") {
              exam.questions.forEach((question) => {
                possibleQuestions.push(question);
              });
              exam.answers.forEach((answer) => {
                possibleAnswers.push(answer);
              });
            }
          });
          break;
        case "math":
          // Do nothing right now as math/english not implemented yet
          break;
        case "eng":
          // Do nothing right now as math/english not implemented yet
          break;
        case "all":
          // Get all exams
          exams.papers.forEach((exam) => {
            // For every question in each exam, add to the possible questions array
            exam.questions.forEach((question) => {
              possibleQuestions.push(question);
            });
            exam.answers.forEach((answer) => {
              possibleAnswers.push(answer);
            });
          });
          break;
        default:
          break;
      }

      // Set loading state to false once finished
      setIsLoading(false);

      // Choose five random indexes from the array generated
      let possibleIndexMax = possibleQuestions.length;
      let randomIndexes = [];

      while (randomIndexes.length < 5) {
        let randomIndex = Math.floor(Math.random() * possibleIndexMax);
        if (randomIndexes.indexOf(randomIndex) === -1) {
          randomIndexes.push(randomIndex);
        }
      }

      // Populate questions and answers ready to submit to state
      let chosenQuestions = [];
      let chosenAnswers = [];
      randomIndexes.forEach((index) => {
        chosenQuestions.push(possibleQuestions[index]);
        chosenAnswers.push(possibleAnswers[index]);
      });

      // Set state to hold these answers and questions
      setQuestions(chosenQuestions);
      setQuestionAnswers(chosenAnswers);
    }
  }, [subjectSelected]);

  const onSelectHandler = (subject) => {
    switch (subject) {
      case "vr":
        setSubjectSelected("vr");
        setStartQuiz(true);
        break;
      case "n-vr":
        setSubjectSelected("n-vr");
        setStartQuiz(true);
        break;
      case "math":
        setSubjectSelected("math");
        setStartQuiz(true);
        break;
      case "eng":
        setSubjectSelected("eng");
        setStartQuiz(true);
        break;
      case "all":
        setSubjectSelected("all");
        setStartQuiz(true);
        break;
      default:
        break;
    }
  };

  const createQuiz = () => {
    let returnStatement = null;
    // Only run the remaining if the array is bigger than 0
    if (questions.length !== 0) {
      returnStatement = questions.map((question, index) => {
        return (
            <QuizQuestion
              key={index}
              question={question}
              questionNumber={index}
              updateAnswers={updateAnswers}
              answers={userAnswers}
              currentAnswer={userAnswers[index]}
            />
        );
      });
    }

    return returnStatement;
  };

  const updateAnswers = (answer, questionNumber) => {
    let currentUserAnswers = userAnswers;
    currentUserAnswers[questionNumber] = answer;
    setUserAnswers(currentUserAnswers);
  };

  // When submit is pressed, finish the exam
  const submitHandler = () => {
    setQuizCompleted(true);
  };

  const resetAllHandler = () => {
    setStartQuiz(false);
    setQuizCompleted(false);
  };

  const quizNotCompletedHandler = () => {
    return (
      <Fragment>
        {!startQuiz && (
          <Fragment>
            <div>
              <h1>Quiz - Choose a Subject</h1>
            </div>
            <p>
              Here you can complete a 5 question quiz to help practise your
              skills in each subject!
            </p>
            <p>
              These should take an average of 4 minutes to complete. Try doing
              questions from all subjects for a real challenge!
            </p>
            <p>
              Have pen and paper ready, it's best to have all your working
              written down!
            </p>
            <div className={classes.exam_list_selectors}>
              <CategoryButton
                onClick={() => onSelectHandler("vr")}
                text="VR"
                subject="vr"
              />
              <CategoryButton
                onClick={() => onSelectHandler("n-vr")}
                text="N-VR"
                subject="n_vr"
              />
              <CategoryButton text="MATH" subject="math" />
              <CategoryButton text="ENGLISH" subject="eng" />
              <CategoryButton
                onClick={() => onSelectHandler("all")}
                text="ALL"
                subject="general"
              />
            </div>
          </Fragment>
        )}
        {startQuiz && isLoading && <h2>Loading...</h2>}
        {startQuiz && <h1>Quiz</h1>}
        {startQuiz && !isLoading && createQuiz()}
        {startQuiz && !isLoading && (
          <AppButton onClick={submitHandler} color="blue" text="Finish" />
        )}
      </Fragment>
    );
  };

  return <Fragment>{!quizCompleted ? quizNotCompletedHandler() : <MarkQuiz resetAll={resetAllHandler} questions={questions} questionAnswers={questionAnswers} userAnswers={userAnswers} />}</Fragment>;
};

export default Quiz;
