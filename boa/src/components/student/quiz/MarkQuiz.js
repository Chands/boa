import { Fragment, useEffect } from "react";
import IncorrectAnswer from "../exam/submission/IncorrectAnswer";
import AppButton from "../../ui/AppButton";
import classes from "../exam/submission/MarkExam.module.css";

const MarkQuiz = (props) => {
  // Scroll to top once done.
  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])

  let questions = props.questions;
  let userAnswers = props.userAnswers;
  let questionAnswers = props.questionAnswers;

  // This will compare each answer to the answers stored in the array
  let totalMark = 0;
  let incorrectAnswers = [];

  // Mark each answer
  userAnswers.forEach((userAnswer, index) => {
    let correctAnswer = questionAnswers[index].answer;

    // If lower case values match, then add to array
    if (userAnswer.toLowerCase() === correctAnswer.toLowerCase()) {
      totalMark++;
    } else {
      // If answer was wrong, add to incorrectAnswers array
      incorrectAnswers.push({
        question: questions[index],
        answer: questionAnswers[index],
        userAnswer: userAnswer,
      });
    }
  });

  const populateAnswers = () => {
    let returnStatement = null;
    returnStatement = incorrectAnswers.map((incorrectAnswer) => (
      <div key={Math.random()} className={classes.answerDiv}>
        <IncorrectAnswer key={Math.random()} answer={incorrectAnswer} questionNumberNeeded={false} />
      </div>
    ));
    return returnStatement;
  };

  // This is for giving the user points for completion
  // Load point total from local storage
  let studentPoints = JSON.parse(localStorage.getItem("points"));
  if (studentPoints == null) {
    // Set to empty array if it doesn't exist
    studentPoints = 0;
  }

  let pointsToGain = 0;
  if (totalMark > 2) {
    pointsToGain = 2;
  } else {
    pointsToGain = 1;
  }

   // Add points to total! useEffect used so it happens only once
   useEffect(() => {
    // Add points to total
    let studentPointsCopy = studentPoints;
    let totalPoints = studentPointsCopy + pointsToGain;
    localStorage.setItem("points", JSON.stringify(totalPoints));
    // eslint-disable-next-line
  }, []);

  return (
    <Fragment>
      <h1>Quiz</h1>
      <h2>Quiz Complete!</h2>
      <p>
        Your mark was: <b>{totalMark} out of 5.</b>
      </p>
      <p>You gained <b>{pointsToGain}</b> points for this activity!</p>
      <h2>Here are the answers you didn't quite get:</h2>
      {populateAnswers()}
      <AppButton onClick={props.resetAll} text="Do Another" color="blue" />
    </Fragment>
  );
};

export default MarkQuiz;
