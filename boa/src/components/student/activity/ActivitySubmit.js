import { Fragment, useState, useEffect } from "react";
import ActivityForm from "./ActivityForm";
import classes from "../../ui/ListItem.module.css";
import "../../ui/colors.css";
import AppButton from "../../ui/AppButton";

const ActivitySubmit = () => {
  // eslint-disable-next-line
  const [studentActivitiesState, setStudentActivitesState] = useState([]);
  // Load activities from local storage
  let studentActivities = JSON.parse(localStorage.getItem("studentActivities"));
  if (studentActivities == null) {
    // Set to empty array if it doesn't exist
    studentActivities = [];
  }

  // This is here so the page re-renders when an activity is added
  useEffect(() => {
    setStudentActivitesState(studentActivities);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const submitActivityHandler = (activity) => {
    // Add activity to local dstorage
    studentActivities.push(activity);
    localStorage.setItem(
      "studentActivities",
      JSON.stringify(studentActivities)
    );

    // Re-render the page - Iupdate the local state to make it do so
    setStudentActivitesState(
      JSON.parse(localStorage.getItem("studentActivities"))
    );
  };

  // For removing an activity from the list
  const removeActivity = (activityToDelete) => {
    // Find the index of the item that needs to be deleted and delete it
    let index = studentActivities.findIndex(
      (activity) => activity === activityToDelete
    );
    studentActivities.splice(index, 1);
    // Update local storage
    localStorage.setItem(
      "studentActivities",
      JSON.stringify(studentActivities)
    );
    // Re-render the page - Iupdate the local state to make it do so
    setStudentActivitesState(
      JSON.parse(localStorage.getItem("studentActivities"))
    );
  };

  const populateActivities = () => {
    let sortedArray = studentActivities;
    sortedArray.sort(function (a, b) {
      // Turn your strings into dates, and then subtract them
      // to get a value that is either negative, positive, or zero.
      return new Date(b.activityDate) - new Date(a.activityDate);
    });

    // With array sorted by date, print each

    let returnStatement = null;
    returnStatement = sortedArray.map((activity, index) => (
      <div key={index} className={`${classes.list_item}`}>
        <div className={`general ${classes.list_item_heading}`}>
          <p className={classes.activityDate}>{activity.activityCompleted}</p>
        </div>
        <div className={`${classes.list_item_detail}`}>
        <p className={classes.activityDate}>{activity.activityDate}</p>
          <div className={classes.inputOptions}>
          <AppButton
              onClick={(e) => removeActivity(activity)}
              color="red"
              text="Remove"
            />
          </div>
        </div>
      </div>
    ));
    return returnStatement;
  };

  return (
    <Fragment>
      <div>
        <h1>Submit an Activity</h1>
        <p>
          Here you can submit activities you have completed that you feel have
          helped you prepare for the Eleven-plus. This includes things like:
        </p>
        <ul>
          <li>Reading a book.</li>
          <li>Completing a set of puzzles.</li>
          <li>Playing an educational game.</li>
          <li>Studying or practising for the exam outside of BOA.</li>
        </ul>
        <p>
          Your parent/guardian/teacher will then approve these activites to net
          you points!
        </p>
      </div>
      <h2>Submit a New Activity</h2>
      <ActivityForm submitActivity={submitActivityHandler} />
      <div>
        <h2>Your pending activities:</h2>
        {populateActivities()}
      </div>
    </Fragment>
  );
};

export default ActivitySubmit;
