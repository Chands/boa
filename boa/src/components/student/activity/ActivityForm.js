import { Fragment, useState } from "react";
import AppButton from '../../ui/AppButton';

const ActivityForm = (props) => {
  const [activityCompletedInput, setActivityCompletedInput] = useState("");
  const [dateCompletedInput, setDateCompletedInput] = useState("")

  const submitActivityHandler = (event) => {
    event.preventDefault();
    console.log(dateCompletedInput);
    let newActivity = {
      activityCompleted: activityCompletedInput,
      activityDate: dateCompletedInput,
    };
    if (
      newActivity.activityCompleted !== "" &&
      newActivity.dateCompleted !== ""
    ) {
        setActivityCompletedInput("");
        setDateCompletedInput("");

        // Submit it to parent
        props.submitActivity(newActivity);
    }
  };

  const activityCompletedInputUpdateHandler = (event) => {
      setActivityCompletedInput(event.target.value);
  }

  const dateCompletedInputUpdateHandler = (event) => {
      setDateCompletedInput(event.target.value);
  }

  return (
    <Fragment>
      <div>
        <form onSubmit={submitActivityHandler} className="form-margin">
          <p>What activity did you complete?</p>
          <input name="activityCompleted" type="text" value={activityCompletedInput} onInput={activityCompletedInputUpdateHandler} className="green-text-input" placeholder="What did you do?"></input>
          <p>When did you complete the activity?</p>
          <input type="date" name="dateCompleted" value={dateCompletedInput} onInput={dateCompletedInputUpdateHandler} className="green-text-input" />
          <AppButton
              onClick={(e) => submitActivityHandler(e)}
              color="blue"
              text="Submit"
            />
        </form>
      </div>
    </Fragment>
  );
};

export default ActivityForm;
