import exams from "../../../assets/exams/exams.json";
import { useState } from "react";
import { Fragment } from "react";
import Question from "./Question";
import AppButton from "../../ui/AppButton";
import Timer from "./timer/Timer";
import classes from "./Exam.module.css";
import { useDispatch } from "react-redux";
import { activeUserActions } from "../../../store/activeUser";

const Exam = (props) => {
  let dispatch = useDispatch();

  // Accept the initial state before the exam begins
  const [startExam, setStartExam] = useState(false);

  // Give a warning before submitting the exam
  const [giveWarning, setGiveWarning] = useState(false);

  // Current question being answered
  const [currentQuestion, setCurrentQuestion] = useState(0);

  // Currently inputted answer
  const [currentAnswer, setCurrentAnswer] = useState("");

  // Update the answer being edited
  const updateAnswerHandler = (answer) => {
    setCurrentAnswer(answer);
  };

  // Answers to questions
  const [answerArray, setAnswerArray] = useState([]);

  // Empty exam to be loaded
  let exam = null;

  // Load exam from localStorage
  exam = exams.papers.find((x) => x.id === props.exam);

  // Max number of questions in test
  const noQuestion = exam.questions.length;

  const submitAnswerHandler = (answer) => {
    setAnswerArray([...answerArray, answer]);

    // Add it to the answers array, if it already exists in array, overwrite it. If the answer is blank, don't overwrite it
    if (answerArray.find((x) => x.number === currentQuestion + 1)) {
      // Check to see if old answer isn't blank
      let blankCheck = answer.answer;

      // If the submitted answer is blank, do nothing
      if (blankCheck !== "") {
        // Let variable hold the old array
        let oldAnswerArray = answerArray;
        // Replace value in array
        oldAnswerArray[currentQuestion] = answer;
        // Replace existing array with new one
        setAnswerArray(oldAnswerArray);
      } else {
        // Do nothing if new submitted answer is blank
        setAnswerArray(answerArray);
      }
    } else {
      // Add a new object to the array, push to end
      setAnswerArray([...answerArray, answer]);
    }
  };

  // When next question, submit the entered answer to the question and set the input back to blank
  const nextQuestionHandler = () => {
    submitAnswerHandler({ number: currentQuestion + 1, answer: currentAnswer });
    setCurrentAnswer("");
    // Make sure not at max number of questions, if at maxmimu number of questions, do not iterate number. This is to prevent 'Enter' advacning a question past the limit
    if (currentQuestion + 1 !== exam.questions.length) {
      let newValue = currentQuestion + 1;
      setCurrentQuestion(newValue);
      setGiveWarning(false);
    } else {
      setGiveWarning(false);
    }
  };

  // When next question, submit the entered answer to the question and set the input back to blank
  const previousQuestionHandler = () => {
    submitAnswerHandler({ number: currentQuestion + 1, answer: currentAnswer });
    setCurrentAnswer("");
    let newValue = currentQuestion - 1;
    setCurrentQuestion(newValue);
    setGiveWarning(false);
  };

  // When the input is changed that shows the current question, go to the next question
  const selectQuestionHandler = (event) => {
    // Trim everything but the numbers from the target
    let str = event.target.value;
    var newQuestionNumber = str.replace(/\D/g, "");
    // Convert to int
    newQuestionNumber = parseInt(newQuestionNumber);
    // Submit the answer entered
    submitAnswerHandler({ number: currentQuestion + 1, answer: currentAnswer });
    setCurrentAnswer("");

    // Check to see if value given is above total, if so set new question to last question
    if (newQuestionNumber > exam.total) {
      newQuestionNumber = exam.total;
    } else if (
      newQuestionNumber <= 0 ||
      isNaN(newQuestionNumber) ||
      newQuestionNumber === ""
    ) {
      // Set to 1 if too small
      newQuestionNumber = 1;
    }
    // Set new question
    setCurrentQuestion(newQuestionNumber - 1);
    setGiveWarning(false);
  };

  // When the submit is first pressed, pass the question's answer to the array
  const warningHandler = () => {
    submitAnswerHandler({ number: currentQuestion + 1, answer: currentAnswer });
    setGiveWarning(true);
  };

  // When submit is pressed, add the current answer to the array and then submit the answer array above
  const submitHandler = () => {
    dispatch(
      activeUserActions.setTakingExam(false)
    );
    submitAnswerHandler({ number: currentQuestion + 1, answer: currentAnswer });
    props.finishExam(answerArray);
  };

  const startExamHandler = () => {
    dispatch(
      activeUserActions.setTakingExam(true)
    );
    setStartExam(true);
  };

  return (
    <Fragment>
      <div>
        <h1>{exam.name}</h1>
      </div>
      {/* Show this before exam starts, encouraging use of paper and the like */}
      {!startExam && (
        <div>
          <p>
            You will have <b>{exam.time}</b> minutes to complete this exam.
          </p>
          <p>
            It will consist of <b>{exam.questions.length}</b> questions with a
            maxmimum score of <b>{exam.total}</b>.
          </p>
          <p>
            Please make sure you have pen and paper ready. We recommend doing
            all of your working out on there!
          </p>
          <AppButton onClick={startExamHandler} color="green" text="Begin" />
        </div>
      )}
      {/* Show this once exam starts */}
      {startExam && (
        <Fragment>
          <div className={classes.qNumberAndTimer}>
            <h2>
              Question Number: {currentQuestion + 1} / {noQuestion}.
            </h2>
            <div className={classes.timerDiv}>
              <Timer examTime={exam.time} submitExam={submitHandler} />
            </div>
          </div>
          <div>
            <Question
              answer={currentAnswer}
              updateAnswer={updateAnswerHandler}
              question={exam.questions[currentQuestion]}
              questionNumber={currentQuestion}
              answerArray={answerArray}
              nextQuestion={nextQuestionHandler}
            />
          </div>
          <div className={classes.examButtons}>
            {/* Set exambutton to be greyed out if question is 0 */}
            {currentQuestion === 0 ? (
              <AppButton onClick={startExamHandler} color="grey" text="-" />
            ) : (
              <AppButton
                onClick={previousQuestionHandler}
                color="green"
                text="Previous"
              />
            )}

            {/* Same as before, but next greyed out if question is max */}
            {currentQuestion === noQuestion - 1 ? (
              <AppButton onClick={startExamHandler} color="grey" text="-" />
            ) : (
              <AppButton
                onClick={nextQuestionHandler}
                color="green"
                text="Next"
              />
            )}

            {!giveWarning ? (
              <AppButton onClick={warningHandler} color="blue" text="Finish" />
            ) : (
              <AppButton
                onClick={submitHandler}
                color="red"
                text="Are you sure?"
              />
            )}
          </div>
          <div className={classes.questionNav}>
            {/* WHen this input is altered, go to the selected question */}
            <h2>Go to: </h2>
            <input
              type="text"
              defaultValue={currentQuestion + 1}
              onInput={selectQuestionHandler}
            />
            <h2>
               / {noQuestion}
            </h2>
          </div>
        </Fragment>
      )}
    </Fragment>
  );
};

export default Exam;
