import { Fragment } from "react";

const Question = (props) => {
  // This is the current input
  const answer = props.answer;

  // This is the previously submitted answer
  let currentAnswer = "";

  const image = props.question.image;
  // const imageMobile = props.question.imageMobile;
  const questionNumber = props.questionNumber;
  const options = props.question.options;
  const primaryInstruction = props.question.primaryInstruction;
  const secondaryInstruction = props.question.secondaryInstruction;
  const textExtract = props.question.textExtract;

  // With the answer array, check to see if the current question has an answer that is not "", if not "" set previous answer to be the previously submitted answer.
  if (props.answerArray.length > 0) {
    if (
      props.answerArray[questionNumber] != null &&
      props.answerArray[questionNumber].answer !== ""
    ) {
      currentAnswer = props.answerArray[questionNumber].answer;
    }
  }

  // Type of question
  const type = props.question.type;

  // Set text input
  const textInputHandler = (event) => {
    props.updateAnswer(event.target.value);
  };

  // Next question when the 'Enter' key is pressed
  const nextQuestionHandler = (event) => {
    event.preventDefault();
    props.nextQuestion();
  }

  // For multiple choice, set the checked option and send off the answer - only allow one checked option, change to blank if re-selected
  const multipleChoiceInputHandler = (event) => {
    if (event.target.value === answer) {
      props.updateAnswer("");
    } else {
      props.updateAnswer(event.target.value);
    }
  };

  // If click the div where the value is contained, then change hte answer
  const multipleChoiceInputHandlerDiv = (value) => {
    if (value === answer) {
      props.updateAnswer("");
    } else {
      props.updateAnswer(value);
    }
  };

  // For multiple choice, this is to display each option
  const populateMultipleChoice = () => {
    let returnStatement = "";
    returnStatement = options.map((option, index) => {
      return (
        <Fragment key={index}>
          <div className="multiple-choice-option-container" onClick={(e) => multipleChoiceInputHandlerDiv(option)} >
            <input
              name={option}
              checked={answer === option}
              type="checkbox"
              value={option}
              id={option}
              onChange={multipleChoiceInputHandler}
              className="checkbox-input"
            />
              <p>{option}</p>
          </div>
        </Fragment>
      );
    });

    return returnStatement;
  };

  // Setup the input options based on the type of question
  const populateInput = () => {
    let returnStatement = null;
    switch (type) {
      case "text":
        // When the input field is exited, submit the answer. Don't allow enter to refresh the page
        returnStatement = (
          <form onSubmit={nextQuestionHandler}>
            <input className="green-text-input"
              onInput={textInputHandler}
              value={answer}
              type="text"
              placeholder="Enter your answer here:"
            />
          </form>
        );
        break;
      case "multipleChoice":
        // For this one, populate the form with a select field with each possible option
        returnStatement = (
          <form onSubmit={(e) => e.preventDefault()}>
            <div className="multiple-choice-container">
              {populateMultipleChoice()}
            </div>
          </form>
        );
        break;
      default:
    }

    return returnStatement;
  };

  return (
    <Fragment>
      <p>{primaryInstruction}</p>
      <div className="questionImageContainer">
        <img
          src={require(`../../../assets/exams${image}`)}
          alt="Question's associated figure."
        ></img>
      </div>
      {/* If text extract exists (for english exams) then put it in */}
      {textExtract !== "" && <p>{textExtract}</p>}
      <p>{secondaryInstruction}</p>
      {/* Answer form */}
      {populateInput()}
      {/* Display previous answer */}
      {currentAnswer !== "" ? <p><b>Your previous answer was:</b> {currentAnswer}</p> : ""}
    </Fragment>
  );
};

export default Question;
