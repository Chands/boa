import { useState, Fragment } from "react";
import classes from "./ExamList.module.css";
import CategoryButton from "../../ui/CategoryButton";
import exams from "../../../assets/exams/exams.json";
import ExamListItem from "./ExamListItem";

const ExamList = (props) => {
  const [selectedTopic, setSelectedTopic] = useState("all");

  const onSelectHandler = (subject) => {
    // Initialize a new topic to select, default is all if button is clicked again
    if (subject === selectedTopic) {
      setSelectedTopic("all");
    } else {
      setSelectedTopic(subject);
    }
  };

  // Set title above the list
  const setHeader = () => {
    let returnStatement = "";
    switch (selectedTopic) {
      case "all":
        returnStatement = "Viewing all exam papers:";
        break;
      case "vr":
        returnStatement = "Viewing Verbal Reasoning papers:";
        break;
      case "n-vr":
        returnStatement = "Viewing Non-Verbal Reasoning papers:";
        break;
      case "math":
        returnStatement = "Viewing Math papers:";
        break;
      case "eng":
        returnStatement = "Viewing English papers:";
        break;
      default:
    }

    return returnStatement;
  };

  // Display items based on the selected subject. For each, give them the chooseExamHandler from parent so they can select and update the selected ID
  const populateList = (subject) => {
    let list = null;
    let filteredList = null;
    switch (selectedTopic) {
      case "all":
        list = exams.papers.map((exam) => {
          return (
            <ExamListItem
              onClick={() => {
                props.chooseExamHandler(exam.id);
              }}
              key={exam.id}
              id={exam.id}
              total = {exam.total}
              name={exam.name}
              time={exam.time}
              subject={exam.subject}
            />
          );
        });
        break;
      case "vr":
        // Create a filtered list that matches the subject
        filteredList = exams.papers.filter((exam) => exam.subject === "vr");

        // Map the filtered list like previous
        list = filteredList.map((exam) => {
          return (
            <ExamListItem
              onClick={() => {
                props.chooseExamHandler(exam.id);
              }}
              key={exam.id}
              id={exam.id}
              total = {exam.total}
              name={exam.name}
              time={exam.time}
              subject={exam.subject}
            />
          );
        });
        break;
      case "n-vr":
        // Create a filtered list that matches the subject
        filteredList = exams.papers.filter((exam) => exam.subject === "n-vr");

        // Map the filtered list like previous
        list = filteredList.map((exam) => {
          return (
            <ExamListItem
              onClick={() => {
                props.chooseExamHandler(exam.id);
              }}
              key={exam.id}
              id={exam.id}
              total = {exam.total}
              name={exam.name}
              time={exam.time}
              subject={exam.subject}
            />
          );
        });
        break;
      case "math":
        // Create a filtered list that matches the subject
        filteredList = exams.papers.filter((exam) => exam.subject === "math");

        // Map the filtered list like previous
        list = filteredList.map((exam) => {
          return (
            <ExamListItem
              onClick={() => {
                props.chooseExamHandler(exam.id);
              }}
              key={exam.id}
              id={exam.id}
              total = {exam.total}
              name={exam.name}
              time={exam.time}
              subject={exam.subject}
            />
          );
        });
        break;
      case "eng":
        // Create a filtered list that matches the subject
        filteredList = exams.papers.filter((exam) => exam.subject === "eng");

        // Map the filtered list like previous
        list = filteredList.map((exam) => {
          return (
            <ExamListItem
              onClick={() => {
                props.chooseExamHandler(exam.id);
              }}
              key={exam.id}
              id={exam.id}
              total = {exam.total}
              name={exam.name}
              time={exam.time}
              subject={exam.subject}
            />
          );
        });
        break;
      default:
    }

    // Check to see if list is empty, if so let the student know
    if (list.length === 0) {
      list = <p>Nothing to display.</p>;
    }

    return list;
  };

  return (
    <Fragment>
      <h1>Take an Examination - Choose Subject</h1>
      <div className={classes.exam_list_selectors}>
        <CategoryButton
          onClick={() => onSelectHandler("vr")}
          text="VR"
          subject="vr"
        />
        <CategoryButton
          onClick={() => onSelectHandler("n-vr")}
          text="N-VR"
          subject="n_vr"
        />
        <CategoryButton
          onClick={() => onSelectHandler("math")}
          text="MATH"
          subject="math"
        />
        <CategoryButton
          onClick={() => onSelectHandler("eng")}
          text="ENGLISH"
          subject="eng"
        />
      </div>
      <div className={classes.exam_list}>
        <h2>{setHeader()}</h2>
        {populateList()}
      </div>
    </Fragment>
  );
};

export default ExamList;
