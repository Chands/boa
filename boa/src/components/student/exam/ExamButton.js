import classes from './ExamButton.module.css'

const ExamButton = (props) => {
  return (
      <div onClick={props.onClick} className={classes.button_container}>
          <p>{props.text}</p>
      </div>
  );
};

export default ExamButton;
