import { Fragment, useEffect } from "react";
import exams from "../../../../assets/exams/exams.json";
import IncorrectAnswer from "./IncorrectAnswer";
import classes from "./MarkExam.module.css";
import AppButton from "../../../ui/AppButton";
import { useHistory } from 'react-router-dom';

const MarkExam = (props) => {
  // Scroll to top once done.
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // The users answers
  const userAnswers = props.answerArray;

  // Empty exam to be loaded
  let exam = null;

  // Load exam from localStorage
  exam = exams.papers.find((x) => x.id === props.exam);

  // Get exam answers
  let examAnswers = exam.answers;

  // This will compare each answer to the answers stored in the json
  let totalMark = 0;
  let incorrectAnswers = [];

  /* For each object in examAnswers, see if a userAnswer of matching question number exists - 
  if it does, check if it's correct and add to totalMark. If it is incorrect or doesn't exist, add it to incorrect answers */
  examAnswers.forEach((examAnswer) => {
    if (userAnswers.find((x) => x.number === examAnswer.number)) {
      // If the answer exists
      // Check if the answer is now correct - setting to lowercase to avoid clashes with uppercases
      let userAnswer = userAnswers.find((x) => x.number === examAnswer.number);
      let userAnswerString = userAnswer.answer;
      if (userAnswerString.toLowerCase() === examAnswer.answer.toLowerCase()) {
        totalMark += 1;
      } else {
        // If not correct, add to the incorrectAnswers array ready to be displayed
        // This is the actual question, this is so the information can be displayed
        let incorrectQuestion = exam.questions.find(
          (x) => x.number === examAnswer.number
        );
        // Push to array, the question information and the answer
        incorrectAnswers.push({
          question: incorrectQuestion,
          answer: examAnswer,
          userAnswer: userAnswer.answer,
        });
      }
    } else {
      // If was not answered, add to the incorrectAnswers array ready to be displayed
      // This is the actual question, this is so the information can be displayed
      let incorrectQuestion = exam.questions.find(
        (x) => x.number === examAnswer.number
      );
      // Push to array, the question information and the answer
      incorrectAnswers.push({
        question: incorrectQuestion,
        answer: examAnswer,
      });
    }
  });

  const populateAnswers = () => {
    let returnStatement = null;
    returnStatement = incorrectAnswers.map((incorrectAnswer) => (
      <div key={Math.random()} className={classes.answerDiv}>
        <IncorrectAnswer key={Math.random()} answer={incorrectAnswer} />
      </div>
    ));
    return returnStatement;
  };

  // This is for giving the user points for completion
  // Load point total from local storage
  let studentPoints = JSON.parse(localStorage.getItem("points"));
  if (studentPoints == null) {
    // Set to empty array if it doesn't exist
    studentPoints = 0;
  }

  // 10 points = 1 hour so divide by 10 and round up
  let timeToPoints = (exam.time / 60) * 10;
  let pointsToGain = Math.ceil(timeToPoints);

  // Submit the exam the local storage, only if the score is higher than a previous attempt.
  let existingExamMarks = JSON.parse(localStorage.getItem("examMarks"));
  if (existingExamMarks == null) {
    // Set to empty array if it doesn't exist
    existingExamMarks = [];
  }

  // Object that will be added to the existingExamMarks array
  let examAndMark = {
    examId: exam.id,
    examMark: totalMark,
  };

  // See if the exam has been completed before
  let previousMark = 0;
  let previousExamAndMark = existingExamMarks.find((x) => x.examId === exam.id);
  if (previousExamAndMark == null) {
    // Add ExamAndMark to the array and set to local storage
    existingExamMarks.push(examAndMark);
    localStorage.setItem("examMarks", JSON.stringify(existingExamMarks));
  } else {
    // Half the point total as it's been done previously
    pointsToGain = Math.ceil(pointsToGain / 2);

    // Check to see if the score is higher
    previousMark = previousExamAndMark.examMark;
    if (totalMark > previousMark) {
      // If the score is higher, replace the existing value in the array
      existingExamMarks.find((x) => x.examId === props.exam).examMark =
        totalMark;

      // If score is higher, also increase point total by 5
      pointsToGain += 5;

      // Set local storage with adjusted value
      localStorage.setItem("examMarks", JSON.stringify(existingExamMarks));
    }
  }

  // If score was above 90% add 1 point
  let ninetyCheck = (totalMark / exam.total) * 100;
  if (ninetyCheck >= 90) {
    pointsToGain += 1;
  }

  // Add points to total! useEffect used so it happens only once
  useEffect(() => {
    // Add points to total
    let studentPointsCopy = studentPoints;
    let totalPoints = studentPointsCopy + pointsToGain;
    localStorage.setItem("points", JSON.stringify(totalPoints));
    // eslint-disable-next-line
  }, []);

  const history = useHistory();

  const goBackHandler = () => {
    history.push('/student/');
  }

  return (
    <Fragment>
      <h1>Exam Complete!</h1>
      <p>
        Your mark was:{" "}
        <b>
          {totalMark} out of {exam.total}
        </b>
        .
      </p>
      <p>
        Your highest mark for this paper is:{" "}
        <b>
          {previousMark} out of {exam.total}
        </b>
        .
      </p>
      <p>
        You gained <b>{pointsToGain} points</b> for this activity!
      </p>
      <h2>Here are the questions you didn't quite get...</h2>
      {populateAnswers()}
      <AppButton onClick={goBackHandler} color="blue" text="Go Back" />
    </Fragment>
  );
};

export default MarkExam;
