import { Fragment } from "react";

const IncorrectAnswer = (props) => {

  // Question information
  const image = props.answer.question.image;
  // const imageMobile = props.answer.question.imageMobile;
  const questionNumber = props.answer.question.number;
  const primaryInstruction = props.answer.question.primaryInstruction;
  const secondaryInstruction = props.answer.question.secondaryInstruction;
  const textExtract = props.answer.question.textExtract;

  // Answer information
  const answer = props.answer.answer.answer;
  const explanation = props.answer.answer.explanation;
  const userAnswer = props.answer.userAnswer;

  // This is for the quiz, to display the question number or not, an exam will need it, a quiz will not
  let questionNumberNeeded = (props.questionNumberNeeded);
  if (questionNumberNeeded == null) {
    questionNumberNeeded = true;
  }
  

  return (
    <Fragment>
      {questionNumberNeeded && <h2>Question Number: {questionNumber}</h2>}
      <p>{primaryInstruction}</p>
      <div className="questionImageContainer">
        <img
          src={require(`../../../../assets/exams${image}`)}
          alt="Question's associated figure."
        ></img>
      </div>
      {/* If text extract exists (for english exams) then put it in */}
      {textExtract !== "" && <p>{textExtract}</p>}
      <p>{secondaryInstruction}</p>
      {(userAnswer != null && userAnswer !== "") && <p><b>Your Answer:</b> {userAnswer}</p>}
      <h2><b>Correct Answer: {answer}</b></h2>
      <p>{explanation}</p>
    </Fragment>
  );
};

export default IncorrectAnswer;
