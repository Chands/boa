import { useState, useEffect } from "react";

const Timer = (props) => {
  const [minutes, setMinutes] = useState(props.examTime);
  const [seconds, setSeconds] = useState(0);

  useEffect(() => {
    let sampleInterval = setInterval(() => {
      if (seconds > 0) {
        setSeconds(seconds - 1);
      }
      if (seconds === 0) {
        if (minutes === 0) {
          props.submitExam();
          clearInterval(sampleInterval);
        } else {
          setMinutes(minutes - 1);
          setSeconds(59);
        }
      }
    }, 1000);
    return () => {
      clearInterval(sampleInterval);
    };
  });

  return (
    <p>
      <b>Time:</b> {minutes < 10 ? `0${minutes}` : minutes}:
      {seconds < 10 ? `0${seconds}` : seconds}
    </p>
  );
};

export default Timer;
