import classes from "./ExamListItem.module.css";
import '../../ui/colors.css';

const ExamListItem = (props) => {

  // Get previously held scores in local storage
  const getPreviousScore = () => {
    // String to be returned
    let returnMark = 0;

    // Check for the examMarks item in local storage
    let existingExamMarks = JSON.parse(localStorage.getItem("examMarks"));
    if(existingExamMarks == null) {
      // If it doesn't exist set the return string to be N/A
      returnMark = "N/A";
    } else {
      let previousMark = "";
      previousMark = existingExamMarks.find(x => x.examId === props.id);
      if (previousMark == null) {
        returnMark = "N/A";
      } else {
        returnMark = previousMark.examMark + " / " + props.total;
      }
    }

    return returnMark;
  };

  return (
    <div className={`${classes.list_item}`} onClick={props.onClick}>
      <div className={`${props.subject} ${classes.list_item_heading}`}>
        <p>{props.name}</p>
      </div>
      <div className={`${classes.list_item_detail}`}>
        <p>{props.time} minutes.</p>
        <p>-</p>
        <p>Highest Mark: {getPreviousScore()}</p>
      </div>
    </div>
  );
};

export default ExamListItem;
