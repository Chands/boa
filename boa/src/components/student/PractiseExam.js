import { useState } from "react";
import { Fragment } from "react";
import ExamList from "./exam/ExamList";
import Exam from "./exam/Exam";
import MarkExam from "./exam/submission/MarkExam";

const PractiseExam = () => {
  const [examSelected, setExamSelected] = useState(false);
  const [examId, setExamId] = useState(null);
  const [answerArray, setAnswerArray] = useState([]);
  const [examCompleted, setExamCompleted] = useState(false);

  const chooseExamHandler = (chosenId) => {
    // Set the examid and set examselected as true
    setExamId(chosenId);
    setExamSelected(true);
  };

  // Finish exam and set state to hold the completed answers
  const finishExamHandler = (answers) => {
    setAnswerArray(answers);
    setExamCompleted(true);
  }

  return (
    <Fragment>
      {/* When an exam not selected, show the exam list */}
      {!examSelected && <ExamList chooseExamHandler={chooseExamHandler} />}
      {/* When an exam has been selected, show the exam!*/}
      {(examSelected && !examCompleted) && <Exam exam={examId} finishExam={finishExamHandler} />}
      {/* Once the exam has been submitted, display the marking */}
      {examCompleted && <MarkExam exam={examId} answerArray={answerArray} />}
    </Fragment>
  );
};

export default PractiseExam;
