import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import classes from "./HeaderNavigation.module.css";
import { Fragment } from "react";
import HeaderNavigation from "./HeaderNavigation";
import { useDispatch } from "react-redux";
import { activeUserActions } from "../../store/activeUser";

const Header = () => {
  let dispatch = useDispatch();

  const activeUser = useSelector((state) => state.activeUser.activeUser);

  // When logo is clicked, set active user to none and ensure parent is logged out
  const returnToHome = () => {
    dispatch(activeUserActions.setActiveUser("none"));
    dispatch(activeUserActions.setParentLoggedIn(false));
  }

  return (
    <Fragment>
      <nav>
        <div className={classes.nav_wrapper}>
          <div className={classes.logo}>
            <Link to="/" onClick={returnToHome}>
              <img
                src={require("../../assets/BOALogo.svg").default}
                alt="BOA Logo and Link to Home"
              />
            </Link>
          </div>
          <div className={classes.navigation}>
            <ul>
              <HeaderNavigation test="tester" activeUser={activeUser} />
            </ul>
          </div>
        </div>
      </nav>
    </Fragment>
  );
};

export default Header;
