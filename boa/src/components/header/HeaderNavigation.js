import { Fragment } from "react";
import { Link } from "react-router-dom";
import { useLocation, Prompt } from "react-router-dom";
import { useDispatch } from "react-redux";
import { activeUserActions } from "../../store/activeUser";
import { useSelector } from "react-redux";

const HeaderNavigation = (props) => {
  const location = useLocation();
  let dispatch = useDispatch();

  // If parent is logged in
  const parentLoggedIn = useSelector(
    (state) => state.activeUser.parentLoggedIn
  );

  // See if taking exam
  let takingExam = useSelector((state) => state.activeUser.takingExam);

  const renderNav = () => {
    // Render the navigation based on the current active user (parent, student, none)
    let returnStatement = <li>Welcome!</li>;
    switch (props.activeUser) {
      case "none":
        returnStatement = <li>Welcome!</li>;
        break;
      case "student":
        returnStatement = (
          <Fragment>
            {location.pathname === "/student" ? (
              <Link
                to="/"
                onClick={() =>
                  dispatch(activeUserActions.setActiveUser("none"))
                }
              >
                <li>Home</li>
              </Link>
            ) : (
              <Link to="/student">
                <li>Back</li>
                {/* If performing an exam, give a warning if they try to leave */}
                {location.pathname === "/student/exam" && takingExam === true && (
                  <Prompt message="Are you sure you want to leave?" />
                )}
              </Link>
            )}
            <Link to="/student/exam">
              <li>Exam</li>
            </Link>
            <Link to="/student/practise">
              <li>Practise</li>
            </Link>
            <Link to="/student/activity">
              <li>Activities</li>
            </Link>
          </Fragment>
        );
        break;
      case "parent":
        returnStatement = (
          <Fragment>
            {location.pathname === "/parent" ? (
              <Link
                to="/"
                onClick={() =>
                  dispatch(
                    activeUserActions.setActiveUser("none"),
                    dispatch(activeUserActions.setParentLoggedIn(false))
                  )
                }
              >
                <li>Home</li>
              </Link>
            ) : (
              <Link to="/parent">
                <li>Back</li>
              </Link>
            )}
            {parentLoggedIn ? (
              <Fragment>
                <Link to="/parent/profile">
                  <li>Profile</li>
                </Link>
                <Link to="/parent/activities">
                  <li>Activities</li>
                </Link>
                <Link to="/parent/password">
                  <li>Password</li>
                </Link>
              </Fragment>
            ) : (
              <span></span>
            )}
          </Fragment>
        );
        break;
      default:
    }
    return returnStatement;
  };

  return <Fragment>{renderNav()}</Fragment>;
};

export default HeaderNavigation;
