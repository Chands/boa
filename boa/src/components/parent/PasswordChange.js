import { Fragment } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useState } from "react";
import { userActions } from "../../store/user";
import { useHistory } from "react-router-dom";
import { activeUserActions } from "../../store/activeUser";
import AppButton from "../ui/AppButton";

const PasswordChange = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  // Get the parent's current login password
  const parentPassword = useSelector((state) => state.user.parentPassword);

  // Set state for current password, new password, and confirmation of this password
  const [currentPassword, setCurrentPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [error, setError] = useState("");

  const currentPasswordHandler = (event) => {
    setCurrentPassword(event.target.value);
  };

  const newPasswordHandler = (event) => {
    setNewPassword(event.target.value);
  };

  const confirmPasswordHandler = (event) => {
    setConfirmPassword(event.target.value);
  };

  const submitNewPasswordHandler = (newPassword) => {
    localStorage.setItem("parentPassword", newPassword);
    dispatch(userActions.setParentPassword(newPassword));
    history.push("/"); // Return user to home panel
    dispatch(activeUserActions.setParentLoggedIn(false)); // Log out user
  };

  const checkPasswordHandler = (event) => {
    event.preventDefault();

    // Form an error message depending on the input
    let errorMessage = "";
    let isError = false;
    if (currentPassword !== parentPassword) {
      errorMessage +=
        "The current password entered does not match the saved password. ";
      isError = true;
    }

    if (newPassword !== confirmPassword) {
      errorMessage += "Your new password doesn't match the confirmation field.";
      isError = true;
    }

    // If no error, then submit new password
    if (isError) {
      // Set the error to be the return of the function
      setError(errorMessage);
    } else {
      setError("");
      // Run other method if all is confirmed
      submitNewPasswordHandler(newPassword);
    }
  };

  return (
    <Fragment>
      <h1>Change your Password</h1>
      <form onSubmit={checkPasswordHandler} className="margin-bottom">
        <p>Please enter your current password:</p>
        <input
          type="text"
          onInput={currentPasswordHandler}
          value={currentPassword}
          className="red-text-input"
        />
        <p>Please enter the new password:</p>
        <input
          type="password"
          onInput={newPasswordHandler}
          value={newPassword}
          className="red-text-input"
        />
        <p>Please confirm your new password:</p>
        <input
          type="password"
          onInput={confirmPasswordHandler}
          value={confirmPassword}
          className="red-text-input"
        />
        <AppButton onClick={checkPasswordHandler} color="blue" text="Submit" />
      </form>
      <h2 className="warning-text">{error}</h2>
    </Fragment>
  );
};

export default PasswordChange;
