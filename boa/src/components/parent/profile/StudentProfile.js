import { Fragment, useState, useEffect } from "react";
import exams from "../../../assets/exams/exams.json";
import ProfileExam from "./ProfileExam";
import classes from "../../student/StudentProfile.module.css";
import snakeImage from "../../../assets/Snake_Child.png";
import AppButton from "../../ui/AppButton";

const StudentProfile = () => {
  // Student points will be here later

  // Load papers from JSON and existing exam marks from local storage
  let examPapers = exams.papers;
  let existingExamMarks = JSON.parse(localStorage.getItem("examMarks"));

  if (existingExamMarks == null) {
    existingExamMarks = [];
  }

  const [returnList, setReturnList] = useState("");
  const [marksAndTotal, setMarksAndTotal] = useState("");

  // Load point total from local storage
  let studentPoints = JSON.parse(localStorage.getItem("points"));
  if (studentPoints == null) {
    // Set to empty array if it doesn't exist
    studentPoints = 0;
  }

  // Levels
  // Each in array represents the threshold to go up a level
  let levels = [
    0, 10, 20, 40, 70, 110, 160, 220, 280, 360, 450, 550, 650, 750, 850, 1000,
  ];
  let studentLevel = 0;
  let toNextLevel = 0;

  // Get player level
  levels.forEach((level, index) => {
    if (studentPoints >= level) {
      studentLevel = index;
      toNextLevel = levels[index + 1] - studentPoints;
    } else {
      // Do nothing
    }
  });

  useEffect(() => {
    // Eight arrays that hold exam mark info
    let vrUserTotal = 0;
    let vrTotal = 0;
    let nvrUserTotal = 0;
    let nvrTotal = 0;
    let mathUserTotal = 0;
    let mathTotal = 0;
    let engUserTotal = 0;
    let engTotal = 0;

    let returnStatement = "";
    returnStatement = examPapers.map((exam) => {
      // Get exam mark for each exam from local storage
      let examMark = existingExamMarks.find(
        (existingExamMark) => existingExamMark.examId === exam.id
      );
      if (examMark == null) {
        // Set to N/A if not found
        examMark = "N/A";
      } else {
        // Add the mark and the total to the respective array
        switch (exam.subject) {
          case "vr":
            vrUserTotal += examMark.examMark;
            vrTotal += exam.total;
            break;
          case "n-vr":
            nvrUserTotal += examMark.examMark;
            nvrTotal += exam.total;
            break;
          case "math":
            mathUserTotal += examMark.examMark;
            mathTotal += exam.total;
            break;
          case "eng":
            engUserTotal += examMark.examMark;
            engTotal += exam.total;
            break;
          default:
            break;
        }
      }
      return (
        <ProfileExam
          key={exam.id}
          id={exam.id}
          total={exam.total}
          time={exam.time}
          name={exam.name}
          subject={exam.subject}
          mark={examMark.examMark}
        />
      );
    });

    setReturnList(returnStatement);
    setMarksAndTotal([
      vrUserTotal,
      vrTotal,
      nvrUserTotal,
      nvrTotal,
      mathUserTotal,
      mathTotal,
      engUserTotal,
      engTotal,
    ]);
    // eslint-disable-next-line
  }, []);

  // Width of level bar
  let barPercentage =
    (studentPoints / (studentPoints + toNextLevel)) * 100 + "%";

  // Determine student title
  let studentTitle = "";
  switch (true) {
    case studentLevel <= 2 && studentLevel >= 0:
      studentTitle = "Hatchling";
      break;
    case studentLevel <= 5 && studentLevel > 2:
      studentTitle = "Python";
      break;
    case studentLevel <= 8 && studentLevel > 5:
      studentTitle = "Kingsnake";
      break;
    case studentLevel <= 11 && studentLevel > 8:
      studentTitle = "Viper";
      break;
    case studentLevel <= 14 && studentLevel > 11:
      studentTitle = "Cobra";
      break;
    case studentLevel === 15:
      studentTitle = "King Cobra";
      break;
    default:
      break;
  }

  return (
    <Fragment>
      <h1>Student Profile:</h1>
      <div
        className={`${classes.backgroundRed} ${classes.container} margin-bottom`}
      >
        {/* <p>Your Profile:</p>
            <p>You have {studentPoints} points!</p>
            <p>You are Level {studentLevel}.</p>
            <p>{toNextLevel} to next level.</p>*/}
        <div className={classes.profileLeft}>
          <div className={classes.profileImageContainer}>
            <img src={snakeImage} alt="The student's avatar" />
          </div>
          <div className={classes.profileTitle}>
            <h1>{studentTitle}</h1>
          </div>
        </div>
        <div className={classes.profileRight}>
          <div className={classes.levelBar}>
            <div
              className={classes.levelBarFill}
              style={{ width: barPercentage }}
            ></div>
            <h2>Level {studentLevel}</h2>
          </div>
          <div className={classes.profileInfo}>
            <h1>Your Profile:</h1>
            <p>You have {studentPoints} points!</p>
            <p>{toNextLevel} to next level.</p>
            <AppButton color="parent" text="Customize" />
          </div>
        </div>
      </div>
      <div>
        <h1>Exam History</h1>
        <h2>Total Scores:</h2>
        <div className={`${classes.totalScoreContainer}`}>
          <div className={`${classes.totalScoreSubject} ${classes.white}`}>
            <Fragment>
              <p>Verbal Reasoning:</p>
              {marksAndTotal[1] !== 0 ? (
                <p>
                  {marksAndTotal[0]} / {marksAndTotal[1]} (
                  {(marksAndTotal[0] / marksAndTotal[1]) * 100}%)
                </p>
              ) : (
                <p>N/A</p>
              )}
            </Fragment>
          </div>
          <div className={`${classes.totalScoreSubject}`}>
            <Fragment>
              <p>Non-verbal Reasoning:</p>
              {marksAndTotal[3] !== 0 ? (
                <p>
                  {marksAndTotal[2]} / {marksAndTotal[3]} (
                  {(marksAndTotal[2] / marksAndTotal[3]) * 100}%)
                </p>
              ) : (
                <p>N/A</p>
              )}
            </Fragment>
          </div>
          <div className={`${classes.totalScoreSubject} ${classes.white}`}>
            <Fragment>
              <p>Maths:</p>
              {marksAndTotal[5] !== 0 ? (
                <p>
                  {marksAndTotal[4]} / {marksAndTotal[5]} (
                  {(marksAndTotal[4] / marksAndTotal[5]) * 100}%)
                </p>
              ) : (
                <p>N/A</p>
              )}
            </Fragment>
          </div>
          <div className={`${classes.totalScoreSubject}`}>
            <Fragment>
              <p>English:</p>
              {marksAndTotal[7] !== 0 ? (
                <p>
                  {marksAndTotal[6]} / {marksAndTotal[7]} (
                  {(marksAndTotal[6] / marksAndTotal[7]) * 100}%)
                </p>
              ) : (
                <p>N/A</p>
              )}
            </Fragment>
          </div>
        </div>
      </div>
      {returnList}
    </Fragment>
  );
};

export default StudentProfile;
