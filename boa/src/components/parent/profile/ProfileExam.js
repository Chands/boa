import classes from "././../../student/exam/ExamListItem.module.css";
import "../../ui/colors.css";

const ProfileExam = (props) => {
  // eslint-disable-next-line
  let id = props.id;
  // eslint-disable-next-line
  let mark = props.mark;
  // eslint-disable-next-line
  let total = props.total;
  // eslint-disable-next-line
  let subject = props.subject;
  // eslint-disable-next-line
  let name = props.name;

  const displayTotalScore = () => {
    let returnStatement = "";
    if (props.mark == null) {
      returnStatement = "N/A";
    } else {
      returnStatement = "Highest Mark: " + props.mark + " / " + props.total;
    }
    return returnStatement;
  };

  return (
    <div className={`${classes.list_item} ${classes.regularPointer}`}>
      <div className={`${props.subject} ${classes.list_item_heading}`}>
        <p>{props.name}</p>
      </div>
      <div className={`${classes.list_item_detail}`}>
        <p>{props.time} minutes.</p>
        <p>-</p>
        <p>{displayTotalScore()}</p>
      </div>
    </div>
  );
};

export default ProfileExam;
