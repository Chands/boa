import { Fragment, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Switch, Route, useRouteMatch } from "react-router-dom";
import { activeUserActions } from "../../store/activeUser";
import ParentLogin from "./ParentLogin";
import PanelButton from "../ui/PanelButton";
import PasswordChange from "./PasswordChange";
import StudentProfile from "./profile/StudentProfile";
import ActivityApprover from "./activity/ActivityApprover";
import AppButton from "../ui/AppButton";
import classes from "./ParentNotification.module.css";

const ParentPanel = () => {
  const dispatch = useDispatch();

  // Update the active user to be a student
  useEffect(() => {
    dispatch(activeUserActions.setActiveUser("parent"));
  }, [dispatch]);

  // Get info if parent is logged in
  const parentLoggedIn = useSelector(
    (state) => state.activeUser.parentLoggedIn
  );

  // Login user
  const loginHandler = () => {
    dispatch(activeUserActions.setParentLoggedIn(true));
  };

  // Set the parentloggedin state to false
  const logoutHandler = () => {
    dispatch(activeUserActions.setParentLoggedIn(false));
  };

  let match = useRouteMatch();

  // For displaying a notification if activities are available
  // Load activities from local storage
  let studentActivities = JSON.parse(localStorage.getItem("studentActivities"));
  if (studentActivities == null) {
    // Set to empty array if it doesn't exist
    studentActivities = [];
  }

  let activityCount = studentActivities.length;
  console.log(activityCount);

  return (
    <Fragment>
      <Switch>
        <Route path={`${match.path}/intro`}>
          <div>
            <h1>What is BOA?</h1>
            <p>
              BOA is a learning support tool designed to offer help with
              preperation for the Eleven-plus. Whilst BOA by itself does not
              offer a sufficient method to prepare for the exam by itself, it
              can be used as a tool alongside other methods of learning to
              increase a student's confidence in the exam.
            </p>
            <p>
              BOA intends to keep the student engaged by offering practise
              papers, small snippets of papers for regular practise alongside a
              points system which student's can use to unlock items and titles
              for their avatar.
            </p>
            <p>
              Try to work with your child in interacting with the system!
              Encourage a regular schedule, congratulate them on any milestones
              they may reach and perhaps even offer external rewards for
              reaching certain accomplishments.
            </p>
          </div>
          <div>
            <h1>Student Panel</h1>
            <h2>Points and Levels</h2>
            <p>
              BOA includes a point system, which gives students points (and
              therefore levels) the more they interact and involve themself with
              the system. Students will gain around 10 points for each hour they
              spend with the system, but this varies depending on the activity.
              The Activites section involves a student submitting activites they
              have completed outside of the system, and parents can approve
              these in order to award points. Try to encourage its use, as this
              system can help both you and the student see how much investment
              they've made in preparing for the Eleven-plus and offer an easy
              way to see a steady track record of their progress. Encourage your
              student to not simply involve themself with the system for the
              sake of it - if you notice likely non-legitimate point gain, you
              will be able to reduce it in the Activity Approve page.
            </p>
            <h2>Exam</h2>
            <p>
              Students can complete a practise Eleven-plus examination. These
              encourage students to use pen and paper, as it's a crucial
              component for examination skills. There is a timer on each exam
              which should be paid attention to. Completion of an exam will give
              the student a number of points based on: The length of the exam,
              if they beat a previous score, or if they recieve a score of 90%
              or above. 90% is an often regulated cutoff for accepted students
              for the Eleven-plus. Students will see explanations for any answer
              they weren't able to answer.
            </p>
            <h2>Quiz</h2>
            <p>
              Students can complete short 5 question quizes in any subject of
              the Eleven-plus. These will contain randomly selected questions
              from BOA's exam papers. These should be encouraged often, even one
              completion a day can go a long way in helping a student's
              progress! Students will gain a small amount of points for their
              completion.
            </p>
            <h2>Activity Submitter</h2>
            <p>
              Students can submit activites they have completed that they feel
              have helped with the Eleven-plus. This could be tutoring,
              completing exams, reading, doing puzzles or the like. As a parent,
              you can then approve/dissapprove these activities. Each activity
              offers 5 points, but you are able to offer more in the Parent
              Panel's activity viewer.
            </p>
            <h1>Parent Panel</h1>
            <h2>Student Profile</h2>
            <p>
              Here you can view a student's point total. Alongside this, you can
              view the student's mark on each exam they have taken. You will see
              a total % for all exams the student has taken, it might provide
              help in identifying which subject areas could use further
              practise!
            </p>
            <h2>Activity Viewer</h2>
            <p>
              This allows you to Approve or Deny activities a student has
              submitted. Take care with these! Do talk to the student about what
              they've accomplished and if concerned, ask how they feel it helps
              them with the exam. 10 points is roughly an hour of exam
              preperation, so use the fields on the page to add or remove points
              as you feel is neccessary.
            </p>
            <h2>Change Password</h2>
            <p>
              The parental section is hidden behind your own password, use this
              form to change your parental password.
            </p>
          </div>
        </Route>
        <Route path={`${match.path}/profile`}>
          <StudentProfile />
        </Route>
        <Route path={`${match.path}/activities`}>
          <ActivityApprover />
        </Route>
        <Route path={`${match.path}/password`}>
          <PasswordChange />
        </Route>
        <Route path={`${match.path}/`}>
          {/* if parent isn't logged in, display the login form */}
          {!parentLoggedIn ? (
            <Fragment>
              <p>You are not logged in.</p>
              <ParentLogin loginHandler={loginHandler} />
            </Fragment>
          ) : (
            <Fragment>
              <h1>Parent Panel</h1>
              <p>What would you like to do?</p>
              <div className="panel-container">
                <PanelButton
                  color="red"
                  text="View student profile."
                  link={`${match.path}/profile`}
                  image="Snake_Icon.svg"
                  alt="Link to view the student's profile"
                />
                {/* SET UP A NOTIFICATION IF THERE IS ONE AVAILABLE */}
                <div className={classes.notificationContainer}>
                  <PanelButton
                    color="red"
                    text="Approve activities."
                    link={`${match.path}/activities`}
                    image="Activity_Icon.svg"
                    alt="Link to view the student's pending activities"
                  />
                  {activityCount > 0 && (
                    <div className={classes.notificationIcon}>
                      <p>{activityCount}</p>
                    </div>
                  )}
                </div>
                <PanelButton
                  color="red"
                  text="Change your password."
                  link={`${match.path}/password`}
                  image="Password_Icon.svg"
                  alt="Link to change parent's password"
                />
                <PanelButton
                  color="red"
                  text="How BOA works."
                  link={`${match.path}/intro`}
                  image="Info_Icon.svg"
                  alt="Link to find information about BOA"
                />
              </div>
              <div className="center-div">
                <AppButton
                  onClick={logoutHandler}
                  color="parent"
                  text="Log Out"
                />
              </div>
            </Fragment>
          )}
        </Route>
      </Switch>
    </Fragment>
  );
};

export default ParentPanel;
