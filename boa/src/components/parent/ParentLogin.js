import { useSelector } from "react-redux";
import { Fragment, useState } from "react";
import AppButton from '../ui/AppButton';

const ParentLogin = (props) => {
  // Get the parent's login password
  const parentPassword = useSelector((state) => state.user.parentPassword);

  // Store value of input
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  // Update the state value of the input once any chance to the input is made
  const inputCheckHandler = (event) => {
    setPassword(event.target.value);
  };

  // Compare the parentPassword from local storage to that of the inputted password
  const checkPassword = (event) => {
    event.preventDefault();
    if (password === parentPassword) {
        // If success, log the parent in
        props.loginHandler();
    } else {
        setError("Incorrect Password - Please try again!");
        setPassword(""); 
    }
  };

  return (
      <Fragment>
    <form onSubmit={checkPassword} className="form-margin">
      <input type="text" onInput={inputCheckHandler} value={password} className="red-text-input" />
      <AppButton
        onClick={checkPassword}
        color="blue"
        text="Submit"
      />
    </form>
    {error === "" ? <span></span> : <h2>{error}</h2>}
    <p>(Default password is 'boa')</p>
    </Fragment>
  );
};

export default ParentLogin;
