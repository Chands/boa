import { useState, useEffect, Fragment } from "react";
import AppButton from "../../ui/AppButton";
import classes from "../../ui/ListItem.module.css";

const ActivityApprover = () => {
  // eslint-disable-next-line
  const [studentActivitiesState, setStudentActivitesState] = useState([]);
  const [pointInput, setPointInput] = useState("");
  const [removePointInput, setRemovePointInput] = useState("");

  // Load activities from local storage
  let studentActivities = JSON.parse(localStorage.getItem("studentActivities"));
  if (studentActivities == null) {
    // Set to empty array if it doesn't exist
    studentActivities = [];
  }

  // This is for giving the user points for completion
  // Load point total from local storage
  let studentPoints = JSON.parse(localStorage.getItem("points"));
  if (studentPoints == null) {
    // Set to empty array if it doesn't exist
    studentPoints = 0;
  }

  console.log(studentPoints);

  // This is here so the page re-renders when an activity is added or removed
  useEffect(() => {
    setStudentActivitesState(studentActivities);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // For removing an activity from after denying
  const denyActivity = (activityToDelete) => {
    // Find the index of the item that needs to be deleted and delete it
    let index = studentActivities.findIndex(
      (activity) => activity === activityToDelete
    );
    studentActivities.splice(index, 1);
    // Update local storage
    localStorage.setItem(
      "studentActivities",
      JSON.stringify(studentActivities)
    );
    // Re-render the page - Iupdate the local state to make it do so
    setStudentActivitesState(
      JSON.parse(localStorage.getItem("studentActivities"))
    );
  };

  // For removing an activity from after denying
  const approveActivity = (activityToDelete) => {
    // Find the index of the item that needs to be deleted and delete it
    let index = studentActivities.findIndex(
      (activity) => activity === activityToDelete
    );
    studentActivities.splice(index, 1);
    // Update local storage
    localStorage.setItem(
      "studentActivities",
      JSON.stringify(studentActivities)
    );

    // Code goes here that adds points!
    let pointsToGain = 5;
    // Add points to total
    let studentPointsCopy = studentPoints;
    let totalPoints = studentPointsCopy + pointsToGain;
    localStorage.setItem("points", JSON.stringify(totalPoints));

    // Re-render the page - Iupdate the local state to make it do so
    setStudentActivitesState(
      JSON.parse(localStorage.getItem("studentActivities"))
    );
  };

  const populateActivities = () => {
    let sortedArray = studentActivities;
    sortedArray.sort(function (a, b) {
      // Turn your strings into dates, and then subtract them
      // to get a value that is either negative, positive, or zero.
      return new Date(b.activityDate) - new Date(a.activityDate);
    });

    // With array sorted by date, print each

    let returnStatement = null;
    returnStatement = sortedArray.map((activity, index) => (
      <div key={index} className={`${classes.list_item}`}>
        <div className={`parent ${classes.list_item_heading}`}>
          <p>{activity.activityCompleted}</p>
        </div>
        <div className={`${classes.list_item_detail}`}>
          <p className={classes.activityDate}>{activity.activityDate}</p>
          <div className={classes.inputOptions}>
            <AppButton
              onClick={(e) => approveActivity(activity)}
              color="green"
              text="Approve"
            />
            <AppButton
              onClick={(e) => denyActivity(activity)}
              color="red"
              text="Remove"
            />
          </div>
        </div>
      </div>
    ));
    return returnStatement;
  };

  const pointInputChangeHandler = (event) => {
    // Only allow numbers
    const value = event.target.value.replace(/\D/g, "");
    setPointInput(value);
  };

  const removePointInputChangeHandler = (event) => {
    // Only allow numbers
    const value = event.target.value.replace(/\D/g, "");
    setRemovePointInput(value);
  };

  const addPoints = (event) => {
    event.preventDefault();
    if (pointInput === "") {
      return;
    }
    // Code goes here that adds points!
    let pointsToGain = parseInt(pointInput);
    // Add points to total
    let studentPointsCopy = studentPoints;
    let totalPoints = parseInt(studentPointsCopy + pointsToGain);
    localStorage.setItem("points", JSON.stringify(totalPoints));

    setPointInput("");

    // Re-render the page - Iupdate the local state to make it do so
    setStudentActivitesState(
      JSON.parse(localStorage.getItem("studentActivities"))
    );
  };

  const removePoints = (event) => {
    event.preventDefault();
    if (removePointInput === "") {
      return;
    }
    // Code goes here that adds points!
    let pointsToLose = parseInt(removePointInput);
    // Minus points to total
    let studentPointsCopy = studentPoints;
    let totalPoints = parseInt(studentPointsCopy - pointsToLose);
    console.log(totalPoints);
    localStorage.setItem("points", JSON.stringify(totalPoints));

    setRemovePointInput("");

    // Re-render the page - Iupdate the local state to make it do so
    setStudentActivitesState(
      JSON.parse(localStorage.getItem("studentActivities"))
    );
  };

  return (
    <Fragment>
      <div>
        <h1>Activity Approval</h1>
        <p>
          Here you can see a list of activites your student / child has
          completed which they have felt have helped them prepare for the
          Eleven-plus.
        </p>
        <p>
          You should encourage activities like reading, puzzle work or
          successful completion of English or Math homework as they have been
          proven to have a direct benefit in the Eleven-plus!
        </p>
        <p>
          Approving activities gives the user a small boost to there point score
          (5 points). If you feel like your child deserves more for their
          progress, use the form at the bottom to offer more points. Usually an
          hour of time will offer 10 points to a student.
        </p>
      </div>
      <div>{populateActivities()}</div>
      <h2>The Student has {studentPoints} points.</h2>
      <div className="margin-bottom">
        <p>Add points to student's total:</p>
        <form onSubmit={addPoints}>
          <input
            type="text"
            value={pointInput}
            onChange={pointInputChangeHandler}
            className="red-text-input"
          />
          <AppButton onClick={addPoints} color="blue" text="Add" />
        </form>
      </div>
      <div>
        <p>Remove points from student's total:</p>
        <form onSubmit={removePoints}>
          <input
            type="text"
            value={removePointInput}
            onChange={removePointInputChangeHandler}
            className="red-text-input"
          />
          <AppButton onClick={removePoints} color="blue" text="Remove" />
        </form>
      </div>
    </Fragment>
  );
};

export default ActivityApprover;
