import classes from "./Footer.module.css";

const Footer = () => {
  return <div className={classes.footer}>
    <p>BOA v0.1.0</p>
  </div>;
};

export default Footer;
