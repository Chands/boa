import classes from './CategoryButton.module.css'
import './colors.css'

const CategoryButton = (props) => {

    return(<div onClick={props.onClick} className={`${classes.button_container} ${props.subject}`}><p>{props.text}</p></div>);
};

export default CategoryButton;