import classes from "./AppButton.module.css";

const AppButton = (props) => {
  // Determine the colour of the button
  let colorClass = "";
  let textClass = "";
  switch (props.color) {
    case "blue":
      colorClass = classes.blueButton;
      textClass = classes.linkTextBlue;
      break;
    case "green":
      colorClass = classes.greenButton;
      textClass = classes.linkTextGreen;
      break;
    case "grey":
      colorClass = classes.greyButton;
      textClass = classes.linkTextGrey;
      break;
    case "red":
      colorClass = classes.redButton;
      textClass = classes.linkTextRed;
      break;
    case "parent":
      colorClass = classes.parentButton;
      textClass = classes.linkTextParent;
      break;
    default:
      break;
  }

  return (
    <div
      className={`${classes.appButtonContainer} ${colorClass}`}
      onClick={props.onClick}
    >
      <p className={textClass}>{props.text}</p>
    </div>
  );
};

export default AppButton;
