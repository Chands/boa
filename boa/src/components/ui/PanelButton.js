import { Link } from "react-router-dom";
import classes from "./PanelButton.module.css";
import snakeChild from "../../assets/Snake_Child.png";
import snakeParent from "../../assets/Snake_Parent.png";
import examIcon from "../../assets/Exam_Icon.svg";
import quizIcon from "../../assets/Quiz_Icon.svg";
import activityIcon from "../../assets/Activity_Icon.svg";
import snakeIcon from "../../assets/Snake_Icon.svg";
import infoIcon from "../../assets/Info_Icon.svg";
import passwordIcon from "../../assets/Password_Icon.svg";

const PanelButton = (props) => {
  // Determine panel's color
  let color = `classes.${props.color}`;
  switch (props.color) {
    case "green":
      color = classes.green;
      break;
    case "red":
      color = classes.red;
      break;
    default:
      break;
  }

  // Determine panel's icon
  let imageSource = "";
  switch (props.image) {
    case "Snake_Child.png":
      imageSource = snakeChild;
      break;
    case "Snake_Parent.png":
      imageSource = snakeParent;
      break;
    case "Exam_Icon.svg":
      imageSource = examIcon;
      break;
    case "Quiz_Icon.svg":
      imageSource = quizIcon;
      break;
    case "Activity_Icon.svg":
      imageSource = activityIcon;
      break;
    case "Snake_Icon.svg":
      imageSource = snakeIcon;
      break;
    case "Info_Icon.svg":
      imageSource = infoIcon;
      break;
    case "Password_Icon.svg":
      imageSource = passwordIcon;
      break;
    default:
      break;
  }

  return (
    <Link to={props.link} className={classes.link}>
      <div className={`${classes.panelButtonContainer} ${color}`}>
        {props.image && (
          <div className={classes.imageContainer}>
            <img src={imageSource} alt={props.alt} />
          </div>
        )}
        <p className={classes.panelText}>{props.text}</p>
      </div>
    </Link>
  );
};

export default PanelButton;
