import { useSelector } from "react-redux";
import { useLocation, Link, Prompt } from "react-router-dom";
import classes from "./HeaderNav.module.css";
import { Fragment, useState } from "react";
import { useDispatch } from "react-redux";
import { activeUserActions } from "../../store/activeUser";
import logo from "../../assets/BOALogo.svg";
import Hamburger from "../../assets/Hamburger_Icon.svg";
import XHamburger from "../../assets/Hamburger_Icon_Closed.svg";

const Header = () => {
  // Screen width
  let width = window.innerWidth;

  // Current location of user
  const location = useLocation();

  let dispatch = useDispatch();

  // If parent is logged in
  const parentLoggedIn = useSelector(
    (state) => state.activeUser.parentLoggedIn
  );

  const activeUser = useSelector((state) => state.activeUser.activeUser);

  // If hamburger menu is opened
  const [showMenu, setShowMenu] = useState(false);
  let hamburgerIcon = null;

  if (showMenu) {
    hamburgerIcon = XHamburger;
  } else {
    hamburgerIcon = Hamburger;
  }

  let hamburgerBackgroundColor = null;

  // Determine background color of hamburger menu depending upon who is logged in
  switch (activeUser) {
    case "none":
      hamburgerBackgroundColor = classes.noUserBg;
      break;
    case "student":
      hamburgerBackgroundColor = classes.studentUserBg;
      break;
    case "parent":
      hamburgerBackgroundColor = classes.parentUserBg;
      break;
    default:
      break;
  }

  // When logo is clicked, set active user to none and ensure parent is logged out
  const returnToHome = () => {
    dispatch(activeUserActions.setActiveUser("none"));
    dispatch(activeUserActions.setParentLoggedIn(false));
  };

  // See if taking exam
  let takingExam = useSelector((state) => state.activeUser.takingExam);

  const renderDesktopNav = () => {
    // Render the navigation based on the current active user (parent, student, none)
    let returnStatement = <p>Welcome!</p>;
    switch (activeUser) {
      case "none":
        returnStatement = <p>Welcome!</p>;
        break;
      case "student":
        returnStatement = (
          <Fragment>
            {location.pathname === "/student" ? (
              <Link
                to="/"
                onClick={() =>
                  dispatch(activeUserActions.setActiveUser("none"))
                }
              >
                <p>Home</p>
              </Link>
            ) : (
              <Link to="/student">
                <p>Back</p>
                {/* If performing an exam, give a warning if they try to leave */}
                {location.pathname === "/student/exam" &&
                  takingExam === true && (
                    <Prompt message="Are you sure you want to leave?" />
                  )}
              </Link>
            )}
            <Link to="/student/exam">
              <p>Exam</p>
            </Link>
            <Link to="/student/practise">
              <p>Practise</p>
            </Link>
            <Link to="/student/activity">
              <p>Activities</p>
            </Link>
          </Fragment>
        );
        break;
      case "parent":
        returnStatement = (
          <Fragment>
            {location.pathname === "/parent" ? (
              <Link
                to="/"
                onClick={() =>
                  dispatch(
                    activeUserActions.setActiveUser("none"),
                    dispatch(activeUserActions.setParentLoggedIn(false))
                  )
                }
              >
                <p>Home</p>
              </Link>
            ) : (
              <Link to="/parent">
                <p>Back</p>
              </Link>
            )}
            {parentLoggedIn ? (
              <Fragment>
                <Link to="/parent/profile">
                  <p>Profile</p>
                </Link>
                <Link to="/parent/activities">
                  <p>Activities</p>
                </Link>
                <Link to="/parent/password">
                  <p>Password</p>
                </Link>
              </Fragment>
            ) : (
              <span></span>
            )}
          </Fragment>
        );
        break;
      default:
    }
    return returnStatement;
  };

  const renderMobileNavCentral = () => {
    // Render the navigation based on the current active user (parent, student, none)
    let returnStatement = <p>Welcome!</p>;
    switch (activeUser) {
      case "none":
        returnStatement = <p>Welcome!</p>;
        break;
      case "student":
        returnStatement = (
          <Fragment>
            {location.pathname === "/student" ? (
              <Link
                to="/"
                onClick={() =>
                  dispatch(activeUserActions.setActiveUser("none"))
                }
              >
                <p>Home</p>
              </Link>
            ) : (
              <Link to="/student" onClick={() => setShowMenu(false)}>
                <p>Back</p>
                {/* If performing an exam, give a warning if they try to leave */}
                {location.pathname === "/student/exam" &&
                  takingExam === true && (
                    <Prompt message="Are you sure you want to leave?" />
                  )}
              </Link>
            )}
          </Fragment>
        );
        break;
      case "parent":
        returnStatement = (
          <Fragment>
            {location.pathname === "/parent" ? (
              <Link
                to="/"
                onClick={() =>
                  dispatch(
                    activeUserActions.setActiveUser("none"),
                    dispatch(activeUserActions.setParentLoggedIn(false))
                  )
                }
              >
                <p>Home</p>
              </Link>
            ) : (
              <Link to="/parent" onClick={() => setShowMenu(false)}>
                <p>Back</p>
              </Link>
            )}
          </Fragment>
        );
        break;
      default:
    }
    return returnStatement;
  };

  const populateHamburgerMenu = () => {
    let returnStatement = null;
    switch (activeUser) {
      case "none":
        break;
      case "student":
        returnStatement = (
          <Fragment>
            <Link to="/student/exam" onClick={() => setShowMenu(!showMenu)}>
              <div className={classes.hamburgerMenuItem}>
                <p>Exam</p>
              </div>
            </Link>
            <Link to="/student/practise" onClick={() => setShowMenu(!showMenu)}>
              <div className={classes.hamburgerMenuItem}>
                <p>Practise</p>
              </div>
            </Link>
            <Link to="/student/activity" onClick={() => setShowMenu(!showMenu)}>
              <div className={classes.hamburgerMenuItem}>
                <p>Activities</p>
              </div>
            </Link>
          </Fragment>
        );
        break;
      case "parent":
        returnStatement = (
          <Fragment>
            {parentLoggedIn && (
              <Fragment>
                <Link to="/parent/profile">
                  <div className={classes.hamburgerMenuItem}>
                    <p>Profile</p>
                  </div>
                </Link>
                <Link to="/parent/activities">
                  <div className={classes.hamburgerMenuItem}>
                    <p>Activities</p>
                  </div>
                </Link>
                <Link to="/parent/password">
                  <div className={classes.hamburgerMenuItem}>
                    <p>Password</p>
                  </div>
                </Link>
              </Fragment>
            )}
          </Fragment>
        );
        break;
      default:
        break;
    }
    return returnStatement;
  };

  return (
    <div className={classes.navContainer}>
      <div className={classes.logoContainer}>
        <Link to="/" onClick={returnToHome}>
          <img src={logo} alt="BOA Logo and link to the home page"></img>
        </Link>
      </div>
      {/* Navigation for desktop - display if above 620px */}
      {width >= 620 ? (
        <div className={classes.desktopNavContainer}>{renderDesktopNav()}</div>
      ) : (
        <Fragment>
          <div className={classes.centralMobileNavContainer}>
            {renderMobileNavCentral()}
          </div>
          <div className={classes.hamburgerContainer}>
            <img
              src={hamburgerIcon}
              alt="Hamburger Icon for mobile navigation"
              onClick={() => setShowMenu(!showMenu)}
            ></img>
          </div>
        </Fragment>
      )}
      {showMenu && (
        <div className={`${classes.hamburgerMenu} ${hamburgerBackgroundColor}`}>
          {populateHamburgerMenu()}
        </div>
      )}
    </div>
  );
};

export default Header;
