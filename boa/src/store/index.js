import { configureStore } from '@reduxjs/toolkit';

import activeUserReducer from './activeUser';
import userReducer from './user';
import examReducer from './exam';

const store = configureStore({
    reducer: {activeUser: activeUserReducer, user: userReducer, exam: examReducer}
});

export default store;