import { createSlice } from "@reduxjs/toolkit";

const initialActiveUserState = {
    activeUser: 'none',
    cookiesAccepted: false,
    parentLoggedIn: false,
    takingExam: false
};

const activeUserSlice = createSlice({
    name: "activeUser",
    initialState: initialActiveUserState,
    reducers: {
        setActiveUser(state, action) {
            state.activeUser = action.payload;
        },
        setCookiesAccepted(state) {
            state.cookiesAccepted = !state.cookiesAccepted;
        },
        setParentLoggedIn(state, action) {
            state.parentLoggedIn = action.payload;
        },
        setTakingExam(state, action) {
            state.takingExam = action.payload;
        }
    }
});

export const activeUserActions = activeUserSlice.actions;

export default activeUserSlice.reducer;