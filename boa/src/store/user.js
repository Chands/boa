import { createSlice } from "@reduxjs/toolkit";

const initialUserState = {
  parentPassword: "",
};

const userSlice = createSlice({
  name: "user",
  initialState: initialUserState,
  reducers: {
    setParentPassword(state, action) {
      state.parentPassword = action.payload;
    },
  },
});

export const userActions = userSlice.actions;

export default userSlice.reducer;
