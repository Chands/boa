import { createSlice } from "@reduxjs/toolkit";

const initialExamState = {
    id: "",
    paper: null,
    answers: []
};

const examSlice = createSlice({
    name: "exam",
    initialState: initialExamState,
    reducers: {
        setId(state, action) {
            state.id = action.payload;
        },
        setPaper(state, action) {
            state.paper = action.payload;
        },
        setAnswers(state, action) {
            /* This one needs to check if answer already exists, replace it if so, otherwise just add it to the array. Object of {question, answer} */
        }
    }
});

export const examActions = examSlice.actions;

export default examSlice.reducer;